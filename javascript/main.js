const ul = document.querySelector(".tabs")
ul.addEventListener("click", (event) => {
   let eventTarget = event.target
   eventTarget.closest("ul").querySelector(".active-tabs").classList.remove("active-tabs")
   eventTarget.classList.add("active-tabs")
   const dataTarget = eventTarget.dataset.tab
   const text = document.querySelector(dataTarget)
   text.closest("ul").querySelector(".active-js").classList.remove("active-js")
   text.classList.add("active-js")
})

let indexArray = 0
const loadMoreBtn = document.querySelector("#load-more")
const imgArray = [{
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer24.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer25.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer26.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer27.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer28.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer29.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer30.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer31.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer32.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer33.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer34.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer25.png",
   dataAttribute: "wordpress"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer26.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer27.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer28.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer29.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer30.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer31.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer32.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer33.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer34.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer25.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer26.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer27.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer28.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer29.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer30.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer31.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer32.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer33.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer34.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer25.png",
   dataAttribute: "graphic-design"
}, {
   title: "Web Design",
   alt: "Web Design",
   src: "./img/Layer26.png",
   dataAttribute: "web-design"
}, {
   title: "Landing Pages",
   alt: "Landing Pages",
   src: "./img/Layer27.png",
   dataAttribute: "landing-pages"
}, {
   title: "Wordpress",
   alt: "Wordpress",
   src: "./img/Layer28.png",
   dataAttribute: "wordpress"
}, {
   title: "Graphic Design",
   alt: "Graphic Design",
   src: "./img/Layer29.png",
   dataAttribute: "graphic-design"
}]
function sliceArray(array, index) {


   return [...array].splice(index, 12)

}
function renderPhoto(list,id){
   console.log(id)
const imagineList = document.querySelector("#imagine-list")
 imagineList.insertAdjacentHTML("beforeend", `${list.map((item) => {
   
    if(id === "all"){      
      return `<li class="image-list-item" data-filter=${item.dataAttribute}><img src=${item.src} alt=${item.alt}></li>`
    }else if(item.dataAttribute !== id){
    
      return `<li class="image-list-item" hidden="true" data-filter=${item.dataAttribute}><img src=${item.src} alt=${item.alt}></li>`
    }else{
      return `<li class="image-list-item"  data-filter=${item.dataAttribute}><img src=${item.src} alt=${item.alt}></li>`}
   
   }).join("")}`)
}

function checkArray(array, index,id = "all") {
   const list = sliceArray(array, index)
   indexArray = index + list.length
    renderPhoto(list,id) 
   // if(id !== "all"){
   //    console.log(list)
   //    const filtered = list.filter(item =>item.dataAttribute === id)
   //    renderPhoto(filtered)
   // }  
}
checkArray(imgArray, indexArray)
loadMoreBtn.addEventListener("click", (event) => {
   const active = document.querySelector(".works-active").id
   checkArray(imgArray, indexArray,active)
   if (indexArray > 24) {
      loadMoreBtn.remove()
   }
})

const filter = document.querySelector("#filter-list")
filter.addEventListener("click", handleFilter)
function handleFilter(event) {
   const id = event.target.id
   if(event.target.classList.contains("item-li-all")){
      event.target.closest("ul").querySelector(".works-active").classList.remove("works-active")
      event.target.classList.add("works-active")
      const list = [...document.getElementsByClassName("image-list-item")]
      console.log(list)
      if (id === "all") {
         list.forEach((item) => {
            item.removeAttribute("hidden")
         })
         return
      }
      list.forEach((item) => {
         item.setAttribute("hidden", true)
         if (item.dataset.filter === id) {
            item.removeAttribute("hidden")
         }
      })
   }
}



// SLIDER
const parentItem = document.querySelector(".what-people-say-container")
const firstUl = document.querySelector(".integer-text")
const secondUl = document.querySelector(".swipe-people")
const firstList = firstUl.children
const secondList = secondUl.children
let interval = setInterval(changeSlider, 3000)
let currentIndex = 0
function changeSlider() {
   secondList[currentIndex].classList.remove("active")
   firstList[currentIndex].hidden = true
   currentIndex++
   if (firstList.length === currentIndex) {
      currentIndex = 0
   }
   secondList[currentIndex].classList.add("active")
   firstList[currentIndex].hidden = false
}

function previousSLide() {
   secondList[currentIndex].classList.remove("active")
   firstList[currentIndex].hidden = true
   currentIndex--
   if (currentIndex === -1) {
      currentIndex = firstList.length - 1
   }
   secondList[currentIndex].classList.add("active")
   firstList[currentIndex].hidden = false
}
parentItem.addEventListener('click', handleSlider)
function handleSlider(event) {
   const eventTarget = event.target
   clearInterval(interval)
   console.log(interval)
   if (eventTarget.classList.contains("scroll-button-left") || eventTarget.classList.contains("left")) {
      previousSLide()
   }
   if (eventTarget.classList.contains("scroll-button-right") || eventTarget.classList.contains("right")) {
      changeSlider()
   }
   if (eventTarget.classList.contains("img-circle")) {
      clickHandleOnSLiderPhoto(event)
   }
}
function clickHandleOnSLiderPhoto(event) {
   const target = event.target
   const li = target.closest("li")
   const id = li.dataset.filter
   currentIndex = +(id.slice(-1))
   if (li.classList.contains("active")) {
      return
   }
   const currentElem = target.closest("ul").querySelector(".active")
   const idElemPhoto = currentElem.dataset.filter
   document.querySelector(idElemPhoto).hidden = true
   currentElem.classList.remove("active")
   const newLi = document.querySelector(id)
   li.classList.add("active")
   newLi.hidden = false
}
const people = document.querySelector("#people")
people.addEventListener("mouseleave",handleLeave)
function handleLeave(event){
   clearInterval(interval)
   interval = setInterval(changeSlider,3000)
   
}
